//
//  FeedViewController.swift
//  Clapper
//
//  Created by Hakan Koşanoğlu on 12.09.2019.
//  Copyright © 2019 appFactory. All rights reserved.
//

import UIKit
import Kingfisher

class FeedViewController: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var feed: Feed?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DataSource.shared().retriveData()
        DataSource.shared().delegate = self
        
        tableview.register(UINib(nibName: "FeedTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        tableview.delegate = self
        tableview.dataSource = self
    }
}

extension FeedViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feed?.feed.videos.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "cell") as! FeedTableViewCell
        
        let index = feed?.feed.videos[indexPath.row]
        
        cell.userNameLabel.text = index?.userName
        
        let url = URL(string: index?.userAvatar ?? "")
        cell.userImageView.kf.setImage(with: url,placeholder: nil,options: [.transition(.fade(0.3))])
        
        cell.setVideoFrame(with: URL(string: index!.mp4URL)!)
        cell.player?.play()
        
        cell.feedDescriptionLabel.text = index?.caption
        
        cell.timeLabel.text = "\(index?.uploadTime ?? 1) min ago"
        
        cell.likeCounterLabel.text = "\(index?.likes ?? 0) likes"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 623
    }
    
    
}

extension FeedViewController: DataSourceDelegate {
    func retriveDataDone(feed: Feed) {
        self.feed = feed
        self.activityIndicator.stopAnimating()
        self.tableview.reloadData()
        
    }
}

