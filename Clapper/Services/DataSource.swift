//
//  DataSource.swift
//  Clapper
//
//  Created by Hakan Koşanoğlu on 12.09.2019.
//  Copyright © 2019 appFactory. All rights reserved.
//

import UIKit
import Alamofire

protocol DataSourceDelegate {
    func retriveDataDone(feed: Feed)
}

extension DataSourceDelegate {
    func retriveDataDone(feed: Feed) {}
}


class DataSource {
    
    private static var sharedInstance : DataSource = {
        let dataSource = DataSource()
        return dataSource
    }()
    
    class func shared() -> DataSource {
        return sharedInstance
    }
    
    var delegate: DataSourceDelegate?
    
    var feedCounter = 0
    var feed: Feed?
    
    private init() {}
    
    func retriveData() {
        
        let url = "https://s3.eu-central-1.amazonaws.com/clapperbasebucket/interview/samplefeed.json"
        
        Alamofire.request(url, method: .get,encoding: JSONEncoding.default).responseJSON {
            response in
            switch response.result {
            case .success:
                let jsonData = try! JSONDecoder().decode(Feed.self, from: response.data!)
                self.feed = jsonData
                for feed in (self.feed?.feed.videos)! {
                    self.downloadVideo(url: URL(string: feed.mp4URL)!, completiton: { (success, url) in
                        if success {
                            feed.mp4URL = url
                            self.feedCounter += 1
                            
                            if self.feedCounter == self.feed?.feed.videos.count {
                                self.delegate?.retriveDataDone(feed: self.feed!)
                            }
                        }
                    })
                }
                break
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func downloadVideo(url: URL,completiton: @escaping (_ success: Bool,_ url:String) -> ()) {
        
        Alamofire.request(url).downloadProgress(closure : { (progress) in
            //            print(progress.fractionCompleted)
        }).responseData{ (response) in
            
            //            print(response)
            //            print(response.result.value!)
            //            print(response.result.description)
            if let data = response.result.value {
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let dataURL = documentsURL.appendingPathComponent("video\(self.feedCounter).mp4")
                do {
                    try data.write(to: dataURL)
                    completiton(true,"\(dataURL)")
                } catch {
                    print("Something went wrong!")
                }
            }
        }
    }
}
