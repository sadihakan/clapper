//
//  FeedTableViewCell.swift
//  Clapper
//
//  Created by Hakan Koşanoğlu on 12.09.2019.
//  Copyright © 2019 appFactory. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class FeedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var likeCounterLabel: UILabel!
    @IBOutlet weak var feedDescriptionLabel: UILabel!
    
    var player: AVPlayer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setVideoFrame(with url: URL) {
        
        player = AVPlayer(url: url)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspectFill
        self.videoView.layer.addSublayer(playerLayer)
        playerLayer.frame = CGRect(origin: .zero, size: videoView.layer.frame.size)
        self.videoView.isHidden = false
        NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(note:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
        
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
            player?.seek(to: CMTime.zero)
            player?.play()
    }
}
