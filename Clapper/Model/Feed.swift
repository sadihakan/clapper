//
//  Feed.swift
//  Clapper
//
//  Created by Hakan Koşanoğlu on 12.09.2019.
//  Copyright © 2019 appFactory. All rights reserved.
//

import Foundation

// MARK: - Feed
struct Feed: Decodable {
    let feed: FeedClass
}

// MARK: - FeedClass
struct FeedClass: Decodable {
    let title: String
    let videos: [Video]
}

// MARK: - Video
class Video: Decodable {
    let videoID: String
    let userAvatar: String
    let userName: String
    let uploadTime, likes: Int
    var mp4URL: String
    let caption: String
    
    enum CodingKeys: String, CodingKey {
        case videoID = "videoId"
        case userAvatar
        case userName
        case uploadTime
        case likes
        case mp4URL = "mp4Url"
        case caption
    }
}

